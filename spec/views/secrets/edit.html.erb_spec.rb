require 'spec_helper'

describe "secrets/edit.html.erb" do
  before(:each) do
    @secret = assign(:secret, stub_model(Secret))
  end

  it "renders the edit secret form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => secrets_path(@secret), :method => "post" do
    end
  end
end
