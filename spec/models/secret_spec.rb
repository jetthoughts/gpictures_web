require 'spec_helper'

describe Secret do
  describe '#like' do
    subject { Fabricate :secret }
    its(:likes_count) { should == 0 }

    VALID_UID = '55ADE930-5FDF-5EC4-8429-15640684C489~F59D91766065777EAACC1B3F4D2EEBB4'
    INVALID_UID = 'invalidUid'
    INVALID_UID_HASH = 'ABDE-EFG~fgghf'

    context "has not been liked before by same uid" do
      it "should increment count of likes with valid uid" do
        expect {
          subject.like VALID_UID
        }.to change(subject, :likes_count).from(0).to(1)
      end

      it "should not increment count of likes with invalid uid" do
        lambda {
          subject.like INVALID_UID
        }.should_not change(subject, :likes_count).from(0).to(1)
      end
      it "should not increment count of likes with invalid hash" do
        lambda {
          subject.like INVALID_UID_HASH
        }.should_not change(subject, :likes_count).from(0).to(1)
      end
    end

    context "has been liked before by same uid" do
      before(:each) do
        subject.like VALID_UID
      end

      it "should not increment count of likes" do
        lambda {
          subject.like VALID_UID
        }.should_not change(subject, :likes_count)
      end
    end
  end
end
