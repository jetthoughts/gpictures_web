require 'spec_helper'

describe Marker do
  context 'with bad coords' do
    BAD_COORDS = [10001.352083, 103.819836]

    subject { CityMarker.create :coords => BAD_COORDS }

    it { should_not be_valid }
    its(:city) { should_not be }
  end

  context 'with Gorlovka coords' do
    GORLOVKA_LL = [48.3071, 38.029633]

    subject { CityMarker.create! :coords => GORLOVKA_LL }

    its(:city){ should == 'Gorlovka, Ukraine' }
  end
  context 'with Singapore coords' do
    SINGAPORE_LL = [1.352083, 103.819836]

    subject { CityMarker.create! :coords => SINGAPORE_LL }

    its(:city){ should == 'Singapore, Singapore' }
  end
end
