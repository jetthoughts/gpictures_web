Fabricator(:secret) do
  text "Secret Text"
  city "Secret City"
  coords [48, 38]
  photo File.new(File.join(File.dirname(__FILE__), 'default.png'))
end
