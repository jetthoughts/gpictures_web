set :rails_env, "production"

set :domain, "deployer@jetthoughts.com"

role :app, domain
role :web, domain

set :branch, "master"
set :user, "deployer"
set :use_sudo, false

set :deploy_to, "/var/www/#{application}"