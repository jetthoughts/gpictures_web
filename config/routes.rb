PostSecret::Application.routes.draw do

  match '/terms' => 'pages#terms'
  match '/about' => 'pages#about'
  match '/privacy' => 'pages#privacy'

  root :to => 'secrets#index'
  match '*path', to: 'secrets#index'

end
