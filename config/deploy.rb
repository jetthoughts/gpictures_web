require './config/boot'
require 'airbrake/capistrano'

set :application, "greeting_cards"
set :repository,  "git+ssh://git@github.com:jetthoughts/greeting_cards.git"

set :rails_env, "production"

set :domain, "deployer@jetthoughts.com"

role :app, domain
role :web, domain

set :branch, "master"
set :user, "deployer"
set :use_sudo, false

set :deploy_to, "/var/www/#{application}"

set :scm, :git

 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
 end

desc "Show hostname"
task :uname do
    run "uname -a"
end

desc "Show log file from remote server."
task :show_log do
  run "tail -f #{current_path}/log/#{rails_env}.log"
end

