window.isCan = (action, subject) ->
  Veel.ability().isCan action, subject

window.isCannot = (action, subject) ->
  Veel.ability().isCannot action, subject
window.classForScope = (scope, expected) ->
  res = ''
  res = 'disabled' if scope == expected
  res

window.picsUrl = (cat_id) ->
  if cat_id
    '/cats/'+ cat_id + '/pics/'
  else
    '/pics/'
window.urlForPic = (id) ->
  "http://#{document.domain}/pics/#{id}"