class Veel.Models.Page extends Backbone.Model
  url: ->
    "/api/pages/#{@id}"

class Veel.Collections.PagesCollection extends Backbone.Collection
  model: Veel.Models.Page
  url: '/api/pages'
