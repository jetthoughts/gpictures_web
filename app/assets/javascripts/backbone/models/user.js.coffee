class Veel.Models.CurrentUser extends Backbone.Model
  url       : '/api/user'

  isSuperadmin : -> @role == 'superadmin'

  isAdmin      : -> @role == 'admin'
    
  isMember     : -> @role == 'member'
  

class Veel.Models.User extends Backbone.Model

class Veel.Collections.UsersCollection extends Backbone.Collection
  model : Veel.Models.User
  url   : '/users'
