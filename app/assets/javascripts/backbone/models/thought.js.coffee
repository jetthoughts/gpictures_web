class Veel.Models.Thought extends Backbone.Model
  base_url: Veel.api_base_url + '/api/secrets/'

  setUrl : (id) ->
    @url =  @base_url + id

  urlWithoutApi : ->
    @url().slice 4

class Veel.Collections.ThoughtsCollection extends Backbone.Collection
  model : Veel.Models.Thought
  scope : 'latest'
  limit : 25
  url   : Veel.api_base_url+'/api/secrets?latest=true'

  initialize : ->
    this.setElement(this.at(0))

  getElement: () ->
    this.currentElement

  setElement: (model) ->
    this.currentElement = model

  next : ->
    this.setElement(this.at(this.indexOf(this.getElement()) + 1))
    this

  prev :  ->
    this.setElement(this.at(this.indexOf(this.getElement()) - 1))
    this

  setUrlWithUser : (user_id) ->
    @url = "/api/users/#{user_id}/thoughts"

  setUrlWithoutUser : ->
    @url = Veel.api_base_url+"/api/secrets?"+@scope + "=true&limit=" + @limit
    @counter_url = Veel.api_base_url+"/api/secrets/counter"
    
  setUrlWithCategory : (cat_id) ->
    @url = Veel.api_base_url+"/api/categories/" + cat_id + "/secrets?"+@scope + "=true&limit=" + @limit
    @counter_url = Veel.api_base_url+"/api/categories/" + cat_id + "/secrets/counter"

  setScope : (sc) ->
    @scope = sc

  getOrFetch : (id, lambda) ->
    local = this.get(id)
    if (local)
      lambda(local)
    else
      remote = new Veel.Models.Thought
      remote.setUrl(id)
      remote.fetch()
      remote.on 'change', () ->
        lambda(remote)

  counter : (reset, callback) ->
    if reset or !@counter_var
      $.ajax
        url : @counter_url
        dataType : 'json'
        success : (data) =>
          @counter_var = parseInt(data)
          callback(@counter_var, reset) if callback?
    @counter_var

  get : (id) ->
    this.where({_id: id})[0]

  urlWithoutApi : ->
    @url.slice 4
  urlWithoutParams : ->
    @url




