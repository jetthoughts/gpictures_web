class Veel.Models.Comment extends Backbone.Model

class Veel.Collections.CommentsCollection extends Backbone.Collection
  model: Veel.Models.Comment

  constructor : (options) ->
    super options
    @on('add', @setupCommentThought)
    @on('reset', @setupThought)

  setupThought : =>
    @each @setupCommentThought

  setupCommentThought : (comment) =>
    comment.thought = @thought
