Veel.Views.Users ||= {}

class Veel.Views.Users.SendInviteView extends Backbone.View
  template : JST["backbone/templates/users/send_invite"]
  className : 'send_invite'

  events :
   'click #send_invite_btn' : 'send' 

  constructor : (options) ->
    super options
    @model = new Veel.Models.User()
    @model.url = ->
      "/api/users/send_invite"

    @model.bind "change:errors", () =>
      this.render()

  send : (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.unset("errors")
    @model.save {},
      success : (user) =>
        @model.clear()
        @$('form')[0].reset()
        @showSuccessNotice()

      error: (user, jqXHR) =>
        if jqXHR.status == 409
          @loaded_model = $.parseJSON(jqXHR.responseText)
          @showInvitedNotice()
        else
          @model.set({errors: $.parseJSON(jqXHR.responseText).errors})

  showSuccessNotice : ->
    text = "Invite sended successfully."
    Veel.showSuccessAlert(text)

  showInvitedNotice : ->
    if @loaded_model.suspend_state == 'active'
      text = "#{@loaded_model.given_name} is already using VYTL! To see their Profile, click here."
    else
      text = "Sorry, #{@loaded_model.given_name} is not eligible to join VYTL."

    Veel.showWarningAlert(text)

  render : ->
    $(@el).html(@template(@model.toJSON()))
    @$("form").backboneLink(@model)

    return this

