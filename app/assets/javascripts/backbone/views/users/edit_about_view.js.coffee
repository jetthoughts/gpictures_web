Veel.Views.Users ||= {}

class Veel.Views.Users.EditAboutView extends Backbone.View
  template : JST["backbone/templates/users/edit_about"]
  className : 'about_form'

  events :
    "click #save_about"   : "update"
    "click .delete_photo" : "deletePhoto"
    "click .back"         : "show"

  constructor : (options) ->
    super(options)

    @model.bind "change:errors", () =>
      this.render()

  show : ->
    Backbone.history.navigate("users/about", true)


  update : (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.unset("errors")

    @$('input[name=authenticity_token]')
      .val($('meta[name=csrf-token]').attr('content'))

    $(document).mask
      closeOnEsc : false
      closeOnClick : false

    @$('form').submit()
    @$('form')
      .bind 'ajax:success', (e, data, status, jqXHR)=>
        $.mask.close()
        @model.set(eval(data))
        @model.trigger('change:all')
        Backbone.history.navigate("/users/about", true)

      .bind 'ajax:error', (e, jqXHR, status, error)=>
        $.mask.close()
        @model.set
          errors : $.parseJSON(jqXHR.responseText).errors

  deletePhoto : =>
    $.ajax
      type : "post"
      url : @model.url()
      data : $.param
        remove_photo : true
        _method : "put"
      dataType : "json"
      success : (data, status, jqXHR) =>
        $('.delete_photo').hide()
        $('.delete_photo').parent().find('.thought_photo').remove()
        @model.set(eval(data))
        @model.trigger('change:all')
        Veel.reloadGrid()

  render : ->
    json = @model.toJSON()
    json.user = @model
    $(@el).html(@template(json))

    return this

