Veel.Views.Users ||= {}

class Veel.Views.Users.AboutView extends Backbone.View
  template : JST["backbone/templates/users/about"]

  events :
    "click .edit_about" : "edit"

  edit : ->
    Backbone.history.navigate('/users/about/edit', true)

  render : ->
    json = @model.toJSON()
    json.user = @model
    $(@el).html(@template(json))

    return this

