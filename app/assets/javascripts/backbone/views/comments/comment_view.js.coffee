Veel.Views.Comments ||= {}

class Veel.Views.Comments.CommentView extends Backbone.View
  template  : JST["backbone/templates/comments/comment"]
  className : 'comment'

  events :
    "click .destroy"      : "destroy"
    "click .edit"         : "edit"
    "click .save"         : "update"
    "click .comment_user" : "user"

  user : (e) =>
    e.preventDefault()
    Backbone.history.navigate(@$('.comment_user').attr('href'), true)

  destroy : ->
    @model.destroy()
    $(@el).slideUp =>
      @remove()
    return false

  edit : ->
    @old_model_values = @model.toJSON()
    @$('div.col3, .edit').addClass 'hide'
    @$('.form.col3, .save').removeClass 'hide'

  update : (e) ->
    e.preventDefault()
    e.stopPropagation()
    @model.save(null,
      success : (comment) =>
        @render()
      error : =>
        @model.set(@old_model_values)
        @render()
    )

  render : =>
    json = @model.toJSON()
    json.comment = @model
    $(@el).html(@template(json))
    @$("form").backboneLink(@model)
    return this
