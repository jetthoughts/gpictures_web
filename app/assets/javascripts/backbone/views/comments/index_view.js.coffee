Veel.Views.Comments ||= {}

class Veel.Views.Comments.IndexView extends Backbone.View
  template  : JST["backbone/templates/comments/index"]
  className : 'comments'

  initialize : ->
    @options.comments.bind('reset', @addAll)
    @options.comments.bind('add', @addSeparate)

  addAll : =>
    @options.comments.each(@addOne)

  addSeparate : (comment, collection, options) =>
    if options.at == 0
      @prependOne(comment)
    else
      @addOne(comment)

  prependOne : (comment) =>
    view = new Veel.Views.Comments.CommentView({model : comment})
    el = $(view.render().el).hide()
    $(@el).prepend(el)
    el.slideDown('slow')

  addOne : (comment) =>
    view = new Veel.Views.Comments.CommentView({model : comment})
    $(@el).append(view.render().el)

  render : =>
    @addAll()
    return this
