Veel.Views.Comments ||= {}

class Veel.Views.Comments.NewView extends Backbone.View
  template : JST["backbone/templates/comments/new"]

  events :
    "click #save_new_comment" : "save"

  constructor : (options) ->
    super(options)
    @model = new @collection.model()
    @model.bind("change:errors", @render)

  save : (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.unset("errors")

    @collection.create @model.toJSON(),
      wait : true
      at   : 0
      success : (comment) =>
        @model.clear()
        @$('form')[0].reset()

      error: (comment, jqXHR) =>
        @model.set({errors: $.parseJSON(jqXHR.responseText).errors})

  render : =>
    $(@el).html(@template(@model.toJSON() ))
    @$("form").backboneLink(@model)
    return this
