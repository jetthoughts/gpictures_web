Veel.Views.Comments ||= {}

class Veel.Views.Comments.WallCommentView extends Backbone.View
  template  : JST["backbone/templates/comments/wall_comment"]
  className : 'comment'

  events :
    "click .comment_user" : "user"

  constructor : (options) ->
    super options
    @model.on('remove', @destroy)
    @model.on('sync:end', @render)

  user : (e) =>
    e.preventDefault()
    Backbone.history.navigate(@$('.comment_user').attr('href'), true)

  destroy : =>
    $(@el).slideUp =>
      @remove()

    return false

  render : =>
    $(@el).html(@template(@model.toJSON() ))
    return this
