Veel.Views.Comments ||= {}

class Veel.Views.Comments.WallIndexView extends Backbone.View
  className : 'comments'

  initialize : ->
    @options.comments.bind('reset', @addAll)
    @options.comments.bind('add', @addSeparate)
    @options.comments.bind('remove', @removeComment)

  comments : (update = false) =>
    if update
      @comments_var = new Veel.Collections.CommentsCollection(@options.comments.first(2))
    else
      @comments_var ||= new Veel.Collections.CommentsCollection(@options.comments.first(2))

  addAll : =>
    if @options.comments.length > 0
      $(@el).html('')
      @comments().each(@addOne) 

  addSeparate : (comment, collection, options) =>
    if options.at == 0
      @renderNewComment(comment)
      @updateCommentsCounter 1
    else
      @addOne(comment)

  renderNewComment : (comment) =>
    @comment_view = new Veel.Views.Comments.WallCommentView({model: comment})
    comment_el = $(@comment_view.render().el).hide()
    $(@el).prepend(comment_el)
    comment_el.slideDown =>
      if @options.comments.length > 2
        @$('.comment:last-child').slideUp ->
          $(@).remove()
      Veel.reloadGrid()

  updateCommentsCounter : (value) =>
    thought = @options.comments.thought
    thought.set('comments_count', thought.get('comments_count')+value)

  addOne : (comment) =>
    view = new Veel.Views.Comments.WallCommentView({model : comment})
    $(@el).append(view.render().el)

  render : =>
    @addAll()
    return this

  removeComment : =>
    @updateCommentsCounter -1
    if @options.comments.length > 0
      $(@el).html('')
      @comments(true).each(@addOne)
