Veel.Views.Thoughts ||= {}

class Veel.Views.Thoughts.ShowView extends Backbone.View
  template : JST["backbone/templates/thoughts/show"]

  events :
    "click .modal-header .destroy" : "destroy"
    "click .modal-header .edit"    : "edit"
    "click .modal-header .close"   : "close"
    "click .user"                  : "user"

  edit : ->
    uri =  Backbone.history.fragment + "/edit"
    Backbone.history.navigate(uri, true)

  user : (e) =>
    e.preventDefault()
    Backbone.history.navigate(@$('.user').attr('href'), true)

  destroy : ->
    @model.destroy()
    @remove()
    @close()

  close : ->
    $.mask.close()
    uri =  Backbone.history.fragment.replace("/#{@model.id}", '')
    Backbone.history.navigate(uri, true)

  render : ->
    json = @model.toJSON()
    json.thought = @model
    $(@el).html(@template(json))

    return this

