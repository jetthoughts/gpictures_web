Veel.Views.Thoughts ||= {}

class Veel.Views.Thoughts.NewView extends Backbone.View
  template  : JST["backbone/templates/thoughts/new"]
  className : 'thought_form'

  events :
    "click #save_new_thought" : "save"
    "click .delete_media"     : "deleteMedia"
    "click .close"            : "close"

  constructor : (options) ->
    super(options)
    @model = new @collection.model()

    @model.bind("change:errors", () =>
      this.render()
    )

  save : (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.unset("errors")

    @$('input[name=authenticity_token]')
      .val($('meta[name=csrf-token]').attr('content'))

    $(document).mask
      closeOnEsc: false
      closeOnClick: false

    @$('form').submit()
    @$('form')
      .bind 'ajax:success', (e, data, status, jqXHR)=>
        $.mask.close()
        data = eval(data)
        @model = @collection.add(data,{at: 0}) if data.user_id == @collection.user_id or !@collection.user_id?
        @close()
        Veel.reloadGrid()

      .bind 'ajax:error', (e, jqXHR, status, error)=>
        @model.set $.parseJSON(jqXHR.responseText)

  render : ->
    $(@el).html(@template(@model.toJSON()))
    @$("form").backboneLink(@model)

    return this

  close : ->
    $.mask.close()
    $(@el).parents('.modal').modal('hide')
    window.history.go(-1)

  deleteMedia : =>
    $('.delete_media').parent().find('.thought_media').remove()
    $('#thought_media_field, .media_cached, .delete_media').remove()
    @model.unset('media_cache')
