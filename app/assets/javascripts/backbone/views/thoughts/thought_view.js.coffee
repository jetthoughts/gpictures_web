Veel.Views.Thoughts ||= {}

class Veel.Views.Thoughts.ThoughtView extends Backbone.View
  template  : JST["backbone/templates/thoughts/thought"]
  className : 'col33'

  events :
    "click .thought" : "show"
    "click .show"    : "show"

  constructor : (options) ->
    super(options)
    @model.on('change:all', @render)
    @model.on('remove', @destroy)
    @model.on('change:comments_count', @renderCommentsCounter)
    @model.comments.on('reset', @renderComments)

  show : (e) ->
    e.preventDefault()
    uri =  Backbone.history.fragment.match(/.*pics/)[0] + "/#{@model.get('_id')}"
    Backbone.history.navigate(uri, true)

  destroy : =>
    $(@el).remove()
    Veel.reloadGrid()

  commentsView : =>
    @comments_view_var ||= new Veel.Views.Comments.WallIndexView(comments: @model.comments)

  renderComments : =>
    @$('.comments_block').html(@commentsView().render().el)
    Veel.reloadGrid()

  renderCommentsCounter : =>
    if @model.get('comments_count') > 2
      @$('.count').removeClass('hide')
    else
      @$('.count').addClass('hide')
    @$('.count a span').text(@model.get('comments_count'))

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @renderComments()
    Veel.Tools.dateFormat(@el, '.date')

    @$('img').load ->
      Veel.reloadGrid()
    $(@).ready ->
      Veel.reloadGrid()
    return this
