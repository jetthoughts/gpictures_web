Veel.Views.Thoughts ||= {}

class Veel.Views.Thoughts.EditView extends Backbone.View
  template  : JST["backbone/templates/thoughts/edit"]
  className : 'thought_form'

  events :
    "click #save_thought"          : "update"
    "click .delete_media"          : "deleteMedia"
    "click .back"                  : "show"
    "click .modal-header .close"   : "close"

  constructor : (options) ->
    super(options)

    @model.bind "change:errors", () =>
      this.render()

  show : ->
    uri =  Backbone.history.fragment.replace('/edit', '')
    Backbone.history.navigate(uri, true)

  update : (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.unset("errors")

    @$('input[name=authenticity_token]')
      .val($('meta[name=csrf-token]').attr('content'))

    $(document).mask
      closeOnEsc : false
      closeOnClick : false

    @$('form').submit()
    @$('form')
      .bind 'ajax:success', (e, data, status, jqXHR)=>
        $.mask.close()
        @model.set(eval(data))
        @model.unset('media_cache')
        @model.trigger('change:all')
        @close()
        Veel.reloadGrid()

      .bind 'ajax:error', (e, jqXHR, status, error)=>
        @model.set $.parseJSON(jqXHR.responseText)

  render : ->
    $(@el).html(@template(@model.toJSON() ))
    @$("form").backboneLink(@model)

    return this

  close : ->
    $.mask.close()
    uri =  Backbone.history.fragment.replace("/#{@model.id}/edit", '')
    Backbone.history.navigate(uri, true)

  deleteMedia : =>
    $.ajax
      type : "post"
      url : @model.url()
      data : $.param
        remove_media : true
        _method : "put"
      dataType : "json"
      success : (data, status, jqXHR) =>
        @hideMediaLinks()
        @model.set(eval(data))
        @model.trigger('change:all')
        Veel.reloadGrid()

  hideMediaLinks : =>
    $('.delete_media').parent().find('.thought_media').remove()
    $('#thought_media_field, .media_cached, .delete_media').remove()
    @model.unset('media_cache')
