Veel.Views.Thoughts ||= {}

class Veel.Views.Thoughts.IndexView extends Backbone.View
  template:       JST["backbone/templates/thoughts/index"]
  offset:         0
  load_next_page: true
  loading:        false
  @setuped:       false

  initialize : ->
    @options.thoughts.on('reset', @addAll)
    @options.thoughts.on('remove', @removeOne)

  setupGrid : ->
    gutter = 12
    min_width = 185
    $('.masonry').masonry
      itemSelector: '.box'
      isAnimated: true
      gutterWidth: gutter
      columnWidth : (containerWidth) ->
        containerWidth -= 2
        num_of_boxes = (containerWidth/min_width | 0)
        box_width = (((containerWidth - (num_of_boxes-1)*gutter) / num_of_boxes) | 0)
        box_width = containerWidth if (containerWidth < min_width)
        $('.box').width(box_width);
        box_width

  reloadGrid: ->
    for timeOut in [100, 1000, 2000]
      setTimeout "$('.masonry').masonry('reload')", timeOut

  loadNextPage : =>
    return if @loading

    @loading = true
    if @load_next_page
      #$('#container').append($('#spinner').html())
      @options.thoughts.fetch
        add  : true
        data : $.param(offset : @offset + @options.thoughts.limit)
        complete: =>
          setTimeout( =>
            @loading = false
            #$('#container').find('#floatingCirclesG').remove()
          , 1000)
        success : (collection, response) =>
          if response.length > 0
            d = new Date();
            res = ''
            self = @
            containerElement = $('#container')
            $(response).each () ->
              thought = new Veel.Models.Thought(@)
              containerElement.append(self.buildThought(thought))
            @offset = @offset +  @options.thoughts.limit

            self.reloadGrid()

          else
            @load_next_page = false

  buildThought : (thought) ->
    res = '<div class="box" data-thought-id="'+ thought.get('_id') + '"><div class="wrap">'
    res += '<span class="pin"><a target="_blank" href="http://pinterest.com/pin/create/button/?url='+ urlForPic(thought.get('_id'))
    res += '&media=' + thought.get('photo_url') + '&description='
    res += thought.get('title') + '" class="pin-it-button" count-layout="none">'
    res += '<img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a></span>'
    res += '<span class="time">'
    res += thought.get('formatted_date')
    res += '</span><h3>' + thought.get('title') + '</h3><img alt="' + thought.get('title') + '" src="' + thought.get('photo_thumb_url') + '"><div class="like"><span class="number">' + thought.get('likes_count') + ' people like it</span><span class="cat"><a href="/cats/' + thought.get('category_id') + '/pics">' + thought.get('category_name') + '</a></span></div></div></div>'
    res

  newThought : ->
    Backbone.history.navigate 'thoughts/new', true

  removeOne : =>
    @showWrapper()
    @updateThoughtsCounter(-1)

  showWrapper : =>
    if @options.thoughts.length == 0
      @$(".add_thought_wrapper").removeClass('hide')

  hideWrapper : =>
    #if @options.thoughts.length > 0
      #@$(".add_thought_wrapper").addClass('hide')

  toggleThoughtMenu : =>
    if isCan('create', Veel.Models.Thought) and (parseInt(@options.thoughts.user_id) == Veel.current_user.id or !@options.thoughts.user_id?)
      @$('.thought_menu').removeClass 'hide'
    else
      @$('.thought_menu').addClass 'hide'

  toggleThoughtWrapper : =>
    if isCan('create', Veel.Models.Thought) and (parseInt(@options.thoughts.user_id) == Veel.current_user.id or !@options.thoughts.user_id?)
      # @showWrapper()
    else
      # @hideWrapper()


  updateThoughtsCounter : (value, reset = false) =>
    counter = parseInt(@$('.thoughts_count span').text())
    value = counter + value unless reset
    #@$('.thoughts_count span').text(value)

  addSeparate : (thought, collection, options) =>
    if options.at == 0
      @prependOne(thought)
      @updateThoughtsCounter(1)
    else
      @addOne(thought)
    @hideWrapper()

  addAll : =>
    res = ''
    @options.thoughts.each (thought) =>
      res += $('<div>').append(@buildThought(thought)).remove().html()

    containerElement = $('#container')
    containerElement.html(res)
    @reloadGrid()

  addOne : (thought) =>
    #view = new Veel.Views.Thoughts.ThoughtView({model : thought})
    #@$('#thoughts').append(view.render().el)

  prependOne : (thought) =>
    view = new Veel.Views.Thoughts.ThoughtView({model : thought})
    @$('#container').prepend(view.render().el)

  @setupScroll : (callback)->
    Veel.Views.Thoughts.IndexView.callback = callback
    return if @setuped
    @setuped = true
    $(window).endlessScroll
      fireOnce     : true
      bottomPixels : Veel.Views.Thoughts.IndexView.calcBottomPixels()
      loader: ''
      intervalFrequency: 1000
      callback     : =>
        Veel.Views.Thoughts.IndexView.callback() if Veel.Views.Thoughts.IndexView.callback

  @calcBottomPixels : ->
    screen.height + $('footer').height() * 2

  render : =>
    $(@el).html(@template(category_id:@options.category_id, scope: @options.thoughts.scope, thoughts: @options.thoughts.toJSON(), user: Veel.current_user ))
    Veel.Views.Thoughts.IndexView.setupScroll(@loadNextPage)
    @addAll()

    return this