Veel.Views.Categories ||= {}

class Veel.Views.Categories.IndexView extends Backbone.View
  template  : JST["backbone/templates/categories/index"]
  className : 'categories'

  initialize : ->
    @options.categories.bind('reset', @addAll)
    @options.categories.bind('add', @addSeparate)
    $("#categories .category").live 'click', () ->
      $("#categories .category.visible").removeClass('visible')
      $(@).addClass('visible')

  addAll : =>
    @addOne(new Veel.Models.Category(_id: null, title: 'All'))
    @options.categories.each(@addOne)

  addSeparate : (comment, collection, options) =>
    if options.at == 0
      @prependOne(comment)
    else
      @addOne(comment)

  prependOne : (comment) =>
    view = new Veel.Views.Categories.CategoryView({model : comment})
    el = $(view.render().el).hide()
    $(@el).prepend(el)
    el.slideDown('slow')

  addOne : (comment) =>
    view = new Veel.Views.Categories.CategoryView({model : comment})
    $(@el).append(view.render().el)

  render : =>
    @addAll()
    return this
