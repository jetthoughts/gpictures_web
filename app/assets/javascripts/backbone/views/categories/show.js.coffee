Veel.Views.Categories ||= {}

class Veel.Views.Categories.CategoryView extends Backbone.View
  template : JST["backbone/templates/categories/show"]

  render : ->
    $(@el).html(@template(@model.toJSON() ))
    return this
