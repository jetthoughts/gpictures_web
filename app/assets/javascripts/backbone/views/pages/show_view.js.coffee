Veel.Views.Pages ||= {}

class Veel.Views.Pages.ShowView extends Backbone.View
  template: JST["backbone/templates/pages/show"]

  events :
    "click .modal-header .close" : "close"

  close : ->
    $.mask.close()
    window.history.back()

  render: ->
    $(@el).html(@template(@model.toJSON() ))
    return this
