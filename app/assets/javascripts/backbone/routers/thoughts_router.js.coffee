class Veel.Routers.ThoughtsRouter extends Backbone.Router
  index_rendered  : false
  user_id         : null
  current_user_id : null
  current_photo : null
  current_category: {title:'All', id: null}

  initialize : (options) ->

    @categories = new Veel.Collections.CategoriesCollection()
    @categories.setUrl()        
    @categories.fetch
      success: () =>
        cats_view = new Veel.Views.Categories.IndexView(categories: @categories)    
        $("#swatch-menu").append(cats_view.render().el)

    @thoughts = new Veel.Collections.ThoughtsCollection()
    @current_user_id = Veel.current_user.id if Veel.current_user?
    $('.button.next').live 'click', (e) =>
      e.preventDefault()
      @thoughts.next()
      @changePhoto()

    $('.button.prev').live 'click', (e) =>
      e.preventDefault()
      @thoughts.prev()
      @changePhoto()
      
    $('#modal').on "hidden", () =>
      @onModalClose()
    $('#modal').on "close", () =>
      @onModalClose()

  routes :
    "pics/new"      : "newThought"
    "pics/index"    : "index"
    "pics/latest"   : "latest"
    "pics/popular"  : "popular"
    "cats/:cat_id/pics/latest"   : "latest"
    "cats/:cat_id/pics/popular"  : "popular"
    "pics/:id/edit" : "edit"
    "pics/:id"      : "show"
    "cats/:cat_id/pics/:id" : "catShow"
    "cats/:id/pics" : "categoryIndex"
    "pics*"         : "index"

    "users/:user_id/pics/new"      : "newThought"
    "users/:user_id/pics/index"    : "index"
    "users/:user_id/pics/:id/edit" : "edit"
    "users/:user_id/pics/:id"      : "show"
    "users/:user_id/pics*"         : "index"

    "my/pics/new"      : "myNewThought"
    "my/pics/index"    : "myIndex"
    "my/pics/:id/edit" : "myEdit"
    "my/pics/:id"      : "myShow"
    "my/pics*"         : "myIndex"

    ""                     : "wrongRoute"
    "*path"                : "wrongRoute"

  myNewThought : ->
    @newThought(@current_user_id)

  myIndex : ->
    @index(@current_user_id)

  myEdit : (id) ->
    @edit(@current_user_id, id)

  myShow : (id) ->
    @show(@current_user_id, id)

  setUser : (args, length) ->
    if args.length > length and args[0]? and args[0] != @user_id
      @user_id = args[0]
      @index_rendered = false
    else if args.length <= length and @user_id?
      @user_id = null
      @index_rendered = false
    
  newThought : ->
    @renderIndex()
    @view = new Veel.Views.Thoughts.NewView(collection: @thoughts)
    $("#modal").html(@view.render().el)
    @modalOpen()

  categoryIndex : (id) =>
    @index_rendered = false if id != @current_category.id
    @current_category.id = id
    @selectCategory()
    $("#modal").modal("hide")
    @renderIndex()

  latest : (cat_id) ->
    @current_category.id = cat_id if cat_id != undefined
    @setUser(arguments, 0)
    $("#modal").modal("hide")
    @index_rendered = false
    @thoughts.setScope 'latest'
    @renderIndex()

  popular : (cat_id) ->
    @current_category.id = cat_id if cat_id != undefined
    @setUser(arguments, 0)
    $("#modal").modal("hide")
    @index_rendered = false
    @thoughts.setScope 'popular'
    @renderIndex()

  index : ->
    @setUser(arguments, 0)
    $("#modal").modal("hide")
    @index_rendered = false if null != @current_category.id
    @current_category.id = null
    @renderIndex()

  catShow : (cat_id, id) =>
    @current_category.id = cat_id
    @selectCategory()
    @show(id)

  selectCategory : =>
    setTimeout(() =>
      cur_car = @categories.get(@current_category.id)
      b = $('#current_category').find('b')
      $('#current_category').html(cur_car.get('title')).append($(b))
    ,1000)

  show : (id) ->
    @renderIndex () =>
      @thoughts.getOrFetch id,
        (thought) =>
          @view   = new Veel.Views.Thoughts.ShowView(model: thought)
          $("#modal .modal-body").html(@view.render().el)
          @modalOpen()
          @thoughts.setElement(thought)

  renderIndex :  (callback) ->
    if !@index_rendered
      @resyncThoughts(callback)
      @index_view = new Veel.Views.Thoughts.IndexView
        category_id: @current_category.id
        thoughts: @thoughts

      $("#content").html $(@index_view.render().el).html()
      @index_view.setupGrid()

      @index_rendered = true
    else
      callback() if callback != undefined

  resyncThoughts : (callback) ->
    @thoughts.user_id = null
    if @current_category.id?      
      @thoughts.setUrlWithCategory @current_category.id
    else      
      @thoughts.setUrlWithoutUser()
    @thoughts.reset([]) if @thoughts.length > 0
    @thoughts.fetch
      success: (a,v) =>
        callback() if callback != undefined


  modalOpen : ->
    $('#modal').modal()

  changePhoto : () =>
    thought = @thoughts.getElement()
    return if thought == undefined
    #view   = new Veel.Views.Thoughts.ShowView(model: thought)
    #$("#thought").html(view.render().el)
    uri =  Backbone.history.fragment.match(/.*pics/)[0] + "/#{thought.get('_id')}"
    Backbone.history.navigate(uri, true)

  wrongRoute : ->
    Backbone.history.navigate("pics", true)
    
  onModalClose : ->
    url = Backbone.history.fragment.match(/.*pics/)[0]
    Backbone.history.navigate(url, true)
