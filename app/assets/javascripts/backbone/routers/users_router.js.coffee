class Veel.Routers.UsersRouter extends Backbone.Router
  initialize : (options) ->
    @users = new Veel.Collections.UsersCollection()

  routes :
    "users/:id/about"          : 'about'
    "users/about"              : 'about'
    "users/about/edit"         : 'editAbout'
    "users/send_invite"        : 'send_invite'
    "users/request_invitation" : 'request_invitation'

  request_invitation : ->
    if isCan 'invite', Veel.Models.User
      Veel.showWarningAlert("You cann't request invitation.")
      return false

    @view = new Veel.Views.Users.RequestInviteView()
    $(".mainbody .veel").html(@view.render().el)

  send_invite : ->
    if isCannot 'invite', Veel.Models.User
      Veel.showAuthorizationError()
      return false

    @view = new Veel.Views.Users.SendInviteView()
    $(".mainbody .veel").html(@view.render().el)

  about : (id) ->
    if !id?
      unless Veel.current_user.isNew()
        user = Veel.current_user
      else
        Veel.showAuthorizationError()
        return false
    else
      user = new @users.model({id: id})
      user.fetch()

    @view = new Veel.Views.Users.AboutView(model: user)
    $(".mainbody .veel").html(@view.render().el)

  editAbout : ->
    if Veel.current_user.isNew()
      Veel.showAuthorizationError()
      return false

    @view = new Veel.Views.Users.EditAboutView(model: Veel.current_user)
    $(".mainbody .veel").html(@view.render().el)


