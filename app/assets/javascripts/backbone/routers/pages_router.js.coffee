class Veel.Routers.PagesRouter extends Backbone.Router
  initialize: (options) ->
    @pages = new Veel.Collections.PagesCollection({})

  routes:
    # "pages/new"      : "newPage"
    # "pages/index"    : "index"
    # "pages/:id/edit" : "edit"
    "pages/:id"      : "show"
    # "pages.*"        : "index"

  # newPage: ->
  #   @view = new Veel.Views.Pages.NewView(collection: @pages)
  #   $("#pages").html(@view.render().el)

  # index: ->
  #   @view = new Veel.Views.Pages.IndexView(pages: @pages)
  #   $("#pages").html(@view.render().el)

  show: (id) ->
    unless page = @pages.get(id)
      page = new @pages.model({id: id})
      page.fetch()
      @pages.add page

    intervalId = setInterval( =>
      unless page.isNew()
        @view = new Veel.Views.Pages.ShowView(model: page)
        $("#page").html(@view.render().el)
        @modalOpen()
        clearInterval(intervalId)
    , 50)

  # edit: (id) ->
  #   page = @pages.get(id)

  #   @view = new Veel.Views.Pages.EditView(model: page)
  #   $("#pages").html(@view.render().el)
  #
  modalOpen : ->
    $("#page").modal("show")
    $("#page").expose
      closeOnEsc: false
      closeOnClick: false
