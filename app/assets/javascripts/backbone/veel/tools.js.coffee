class Veel.Tools
  @truncate_range: 20

  @truncate: (text, characters) ->
    if text.length > characters+@truncate_range
      text = text.trim().substring(0, characters)
        .split(' ').slice(0, -1)
        .join(' ') + '...'
    text

  @dateFormat: (el, selector) ->
    date = $(el).find(selector)
    date.text($.format.date(date.text(), 'dd/MM/yy HH:mm'))

  @randomAddition: ->
    parseInt(Math.random()*10000000)
