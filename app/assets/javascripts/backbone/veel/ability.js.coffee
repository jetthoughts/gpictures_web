class Veel.Ability

  constructor : (@user) ->


  isCan : (action, subject) ->
    return @thoughtRule action, subject if subject instanceof Veel.Models.Thought
    return @commentRule action, subject if subject instanceof Veel.Models.Comment

    if subject instanceof Veel.Collections.ThoughtsCollection or
      subject is Veel.Models.Thought
        return @thoughtsRule action, subject

    if subject instanceof Veel.Collections.CommentsCollection or
      subject is Veel.Models.Comment
        return @commentsRule action, subject

    if subject instanceof Veel.Collections.UsersCollection or
      subject is Veel.Models.User
        return @usersRule action, subject

    false

  isCannot : (action, subject) ->
    !@isCan action, subject

  usersRule : (action, users) ->
    return true if @isRead(action)
    return true if @isUser() and action == 'invite'
    return true if @isAdmin()
    false

  commentRule : (action, comment) ->
    return true if @isRead(action)
    # Comment owner abilities
    return true if @isUser() and comment.get('user_id') == @user.get('id')
    # Comment thought owner abilities
    return true if @isUser() and action == 'delete' and comment.thought.get('user_id') == @user.get('id')
    # Admins abilities
    return true if @isAdmin()
    false

  thoughtRule : (action, thought) ->
    return true if @isRead(action)
    # Thought owner abilities
    return true if @isUser() and thought.get('user_id') == @user.get('id')
    # Admins abilities
    return true if @isAdmin()
    false

  thoughtsRule : (action, thoughts) ->
    return true if @isUser() and action == 'create'
    return true if @isRead(action)
    return true if @isAdmin()
    false

  commentsRule : (action, comments) ->
    return true if @isUser() and action == 'create'
    return true if @isRead(action)
    return true if @isAdmin()
    false

  isUser : ->
    @user? and !@user.isNew()

  isRead : (action) ->
    $.inArray(action, ['show','index']) != -1

  isAdmin : ->
    @user and $.inArray(@user.get('role'), ['admin', 'superadmin']) != -1
